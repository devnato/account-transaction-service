# Account-transactions
API rest to handle account operations
The diagram below exemplifies the account/transfer endpoint flow:
![Screenshot](Account-transaction-diagram.png)
