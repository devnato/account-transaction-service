package com.account.application.ports.output;

import com.account.domain.model.Transaction;
import com.account.domain.model.TransactionPageable;

import java.util.List;

public interface TransactionOutputPort {

    Transaction saveTransaction(Transaction transaction);

    TransactionPageable listTransactions(Double amount, int pageNo, int pageSize, String sortBy, String sortDir);

    List<Transaction> findByPaymentInfoId(String paymentInfoId);

    List<Transaction> findByTransferId(String transfer);
}
