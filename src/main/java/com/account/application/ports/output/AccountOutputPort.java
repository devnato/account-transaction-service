package com.account.application.ports.output;

import com.account.domain.model.Account;

import java.util.List;

public interface AccountOutputPort {

    Account saveAccount(Account account);

    Account findByUserId(Long userId);

    List<Account> listAccounts();


}
