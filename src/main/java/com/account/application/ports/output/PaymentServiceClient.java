package com.account.application.ports.output;

import com.account.infrastructure.adapters.output.api.request.PaymentRequest;
import com.account.infrastructure.adapters.output.api.response.PaymentResponse;
import org.springframework.web.bind.annotation.RequestBody;

public interface PaymentServiceClient {

    PaymentResponse payment(@RequestBody PaymentRequest paymentRequest);

}
