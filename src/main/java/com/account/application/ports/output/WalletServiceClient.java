package com.account.application.ports.output;

import com.account.infrastructure.adapters.output.api.request.TransferRequest;
import com.account.infrastructure.adapters.output.api.response.TransferResponse;
import com.account.infrastructure.adapters.output.api.response.WalletBalanceResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface WalletServiceClient {

    WalletBalanceResponse balance(@PathVariable("user_id") Long userId);

    TransferResponse transactions(@RequestBody TransferRequest transferRequest);

}
