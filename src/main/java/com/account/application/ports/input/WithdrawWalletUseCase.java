package com.account.application.ports.input;

import com.account.domain.model.Transaction;

public interface WithdrawWalletUseCase {

    void withdraw(Transaction transaction);

}
