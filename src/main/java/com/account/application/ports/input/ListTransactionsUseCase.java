package com.account.application.ports.input;

import com.account.domain.model.TransactionPageable;

public interface ListTransactionsUseCase {

    TransactionPageable listAccounts(Double amount, int pageNo, int pageSize, String sortBy, String sortDir);

}
