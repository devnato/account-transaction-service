package com.account.application.ports.input;

import com.account.domain.model.PaymentTransferStatus;
import com.account.domain.model.PaymentWebhook;

public interface ProcessPaymentWebhookUseCase {

    PaymentTransferStatus proccessPaymentWebhook(PaymentWebhook paymentWebhook);

}
