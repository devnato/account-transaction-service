package com.account.application.ports.input;

import com.account.domain.model.Transaction;
import com.account.domain.model.TransferData;

public interface TransferAccountUseCase {

    Transaction transferToBankAccount(TransferData transferData);

}
