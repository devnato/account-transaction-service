package com.account.application.ports.input;

import com.account.domain.model.Transaction;

public interface RollbackWithdrawWalletUseCase {

    void rollbackWithdraw(Transaction transaction);

}
