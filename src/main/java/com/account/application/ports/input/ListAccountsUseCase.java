package com.account.application.ports.input;

import com.account.domain.model.Account;

import java.util.List;

public interface ListAccountsUseCase {

    List<Account> listAccounts();

}
