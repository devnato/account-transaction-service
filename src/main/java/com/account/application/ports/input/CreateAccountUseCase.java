package com.account.application.ports.input;

import com.account.domain.model.Account;

public interface CreateAccountUseCase {

    Account createAccount(Account account);

}
