package com.account.application.ports.input;

import com.account.domain.model.TransferStatus;

public interface GetTransferStatusUseCase {

    TransferStatus getTransferStatus(String transferId);

}
