package com.account.domain.service;

import com.account.application.ports.input.*;
import com.account.application.ports.output.AccountOutputPort;
import com.account.application.ports.output.PaymentServiceClient;
import com.account.application.ports.output.TransactionOutputPort;
import com.account.application.ports.output.WalletServiceClient;
import com.account.domain.model.*;
import com.account.infrastructure.adapters.output.api.exception.PaymentDeclinedException;
import com.account.infrastructure.adapters.output.api.exception.PaymentRequestException;
import com.account.infrastructure.adapters.output.api.request.PaymentRequest;
import com.account.infrastructure.adapters.output.api.request.TransferRequest;
import com.account.infrastructure.adapters.output.api.response.PaymentResponse;
import com.account.infrastructure.adapters.output.api.response.TransferResponse;
import com.account.infrastructure.adapters.output.api.response.WalletBalanceResponse;
import com.account.infrastructure.adapters.output.mapper.AccountApiMapper;
import com.account.infrastructure.adapters.output.mapper.PaymentApiMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@AllArgsConstructor
public class AccountService implements CreateAccountUseCase,
                                        TransferAccountUseCase,
                                        ListAccountsUseCase,
                                        ListTransactionsUseCase,
                                        GetTransferStatusUseCase,
                                        ProcessPaymentWebhookUseCase
                                        {

    private final AccountOutputPort accountOutputPort;
    private final TransactionOutputPort transactionOutputPort;
    private final WalletServiceClient walletServiceClient;
    private final PaymentServiceClient paymentServiceClient;
    private final AccountApiMapper accountApiMapper;
    private final PaymentApiMapper paymentApiMapper;

    @Override
    public Account createAccount(Account account) {
        return accountOutputPort.saveAccount(account);
    }

    @Override
    public List<Account> listAccounts() {
        return accountOutputPort.listAccounts();
    }

    @Override
    public Transaction transferToBankAccount(TransferData transferData) {
        Account account = accountOutputPort.findByUserId(transferData.getUserId());
        Transaction transaction = this.mapperToTransaction(transferData);

        if (transferData.getAmount() <= 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Amount must be a positive value");

        withdraw(transferData);

        PaymentRequest paymentRequest = paymentApiMapper.toPaymentRequest(account, transferData);
        try {
            PaymentResponse paymentResponse = paymentServiceClient.payment(paymentRequest);
            transaction.setPaymentInfoId(paymentResponse.getPaymentInfo().getId());
            saveTransactionProcessing(transaction);
        } catch (PaymentRequestException ex) {
            System.out.println(ex.getMessage()); //TODO add log factory
            transaction.setPaymentInfoId(ex.getPaymentId());
            rollbackTransfer(transaction);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something bad happened");
        } catch (PaymentDeclinedException ex) {
            System.out.println(ex.getMessage()); //TODO add log factory
            transaction.setPaymentInfoId(ex.getPaymentId());
            rollbackTransfer(transaction);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Payment declined");
        }
        return transaction;
    }

    private void rollbackTransfer(Transaction transaction) {
        TransferData transferData = TransferData.builder()
                .amount(transaction.getAmount())
                .userId(transaction.getUserId())
                .build();

        saveTransactionFailed(transaction);
        rollbackWithdraw(transferData);
        transaction.setRefund(transferData.getAmount());
        saveRefundTransaction(transaction);
    }

    private Transaction mapperToTransaction(TransferData transferData) {
        double amountFee = transferData.getAmount() * 0.1;
        BigDecimal valueB = BigDecimal.valueOf(amountFee);
        amountFee = valueB.setScale(2, RoundingMode.HALF_UP).doubleValue();
        return Transaction.builder()
                .amount(transferData.getAmount())
                .fee(amountFee)
                .amountWithFeeDiscount(transferData.getAmount() - amountFee)
                .date(new Date())
                .userId(transferData.getUserId())
                .transferId(UUID.randomUUID().toString())
                .build();
    }

    private void withdraw(TransferData transferData) {
        WithdrawData withdrawData = accountApiMapper.toWithdrawData(transferData);
        WalletBalanceResponse walletBalanceResponse = walletServiceClient.balance(withdrawData.getUserId());
        if (walletBalanceResponse.getBalance() < withdrawData.getAmount())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "insufficient funds");
        TransferRequest transferRequest = accountApiMapper.toTransferRequest(withdrawData);
        transferRequest.setAmount(transferRequest.getAmount() * -1);
        TransferResponse transferResponse = walletServiceClient.transactions(transferRequest);
        if (transferResponse == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something bad happened");
        }
    }

    private void rollbackWithdraw(TransferData transferData) {
        TransferRequest transferRequest = accountApiMapper.toTransferRequest(transferData);
        TransferResponse transferResponse = walletServiceClient.transactions(transferRequest);
        if (transferResponse == null) {
            System.out.println("Problem to refund to wallet");
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Something bad happened");
        }
    }

    private void saveRefundTransaction(Transaction transaction) {
        transaction.setId(null);
        transaction.setStatus("REFUNDED_TO_WALLET");
        transaction.setType("REFUND_FROM_BANK_TRANSFER");
        transaction.setDate(new Date());
        transactionOutputPort.saveTransaction(transaction);
    }

    private void saveTransactionFailed(Transaction transaction) {
        transaction.setId(null);
        transaction.setStatus("TRANSACTION_FAILED");
        transaction.setType("TRANSFER_TO_ACCOUNT");
        transaction.setDate(new Date());
        transactionOutputPort.saveTransaction(transaction);
    }

    private void saveTransactionCompleted(Transaction transaction) {
        transaction.setId(null);
        transaction.setStatus("TRANSACTION_COMPLETED");
        transaction.setType("TRANSFER_TO_ACCOUNT");
        transaction.setDate(new Date());
        transactionOutputPort.saveTransaction(transaction);
    }

    private void saveTransactionProcessing(Transaction transaction) {
        transaction.setId(null);
        transaction.setStatus("TRANSACTION_PROCESSING");
        transaction.setType("TRANSFER_TO_ACCOUNT");
        transaction.setDate(new Date());
        transactionOutputPort.saveTransaction(transaction);
    }

    @Override
    public TransactionPageable listAccounts(Double amount, int pageNo, int pageSize, String sortBy, String sortDir) {
        return transactionOutputPort.listTransactions(amount, pageNo, pageSize, sortBy, sortDir);
    }

    @Override
    public PaymentTransferStatus proccessPaymentWebhook(PaymentWebhook paymentWebhook) {
        if (paymentWebhook.getType().equals("payment.succeeded")) {
            return completeTransferSuccess(paymentWebhook);
        } else if (paymentWebhook.getType().equals("payment.fail")) {
            return completeTransferFail(paymentWebhook);
        }
        return null;
    }

    private PaymentTransferStatus completeTransferSuccess(PaymentWebhook paymentWebhook) {
        List<Transaction> transactionList = transactionOutputPort.findByPaymentInfoId(
                paymentWebhook.getData().getPaymentInfoId());

        if (transactionList.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for paymentInfoId");

        if (!isTransferCompleted(transactionList)) {
            Transaction transactionProcessing = getTransactionProcessing(transactionList);
            this.saveTransactionCompleted(transactionProcessing);
        } else
            System.out.println("Transaction already completed, paymentInfoId:" +
                    paymentWebhook.getData().getPaymentInfoId());
        return PaymentTransferStatus.builder()
                .paymentInfoId(paymentWebhook.getData().getPaymentInfoId())
                .status("TRANSACTION_COMPLETED")
                .build();
    }

    private Transaction getTransactionProcessing(List<Transaction> transactionList) {
        return transactionList.stream().
                filter(t -> t.getStatus().equals("TRANSACTION_PROCESSING")).findFirst()
                .orElse(null);
    }

    private PaymentTransferStatus completeTransferFail(PaymentWebhook paymentWebhook) {
        List<Transaction> transactionList = transactionOutputPort.findByPaymentInfoId(
                paymentWebhook.getData().getPaymentInfoId());

        if (transactionList.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found for paymentInfoId");

        if (!isTransferCompleted(transactionList)) {
            Transaction transactionProcessing = getTransactionProcessing(transactionList);
            saveTransactionFailed(transactionProcessing);
            TransferData transferData = TransferData.builder()
                    .amount(transactionProcessing.getAmount())
                    .userId(transactionProcessing.getUserId())
                    .build();
            rollbackWithdraw(transferData);
            transactionProcessing.setRefund(transferData.getAmount());
            saveRefundTransaction(transactionProcessing);
        } else
            System.out.println("Transaction already completed, paymentInfoId:" +
                    paymentWebhook.getData().getPaymentInfoId());
        return PaymentTransferStatus.builder()
                .paymentInfoId(paymentWebhook.getData().getPaymentInfoId())
                .status("TRANSACTION_COMPLETED")
                .build();
    }

    private boolean isTransferFinished(List<Transaction> transactionList) {
        Predicate<Transaction> p = t -> t.getStatus().equals("TRANSACTION_FAILED");
        Predicate<Transaction> p2 = t -> t.getStatus().equals("TRANSACTION_COMPLETED");
        return transactionList.stream().anyMatch(p.or(p2));
    }

    private boolean isTransferFailed(List<Transaction> transactionList) {
        return transactionList.stream().map(Transaction::getStatus)
                .anyMatch("TRANSACTION_FAILED"::equals);
    }

    private boolean isTransferCompleted(List<Transaction> transactionList) {
        return transactionList.stream().map(Transaction::getStatus)
                .anyMatch("TRANSACTION_COMPLETED"::equals);
    }

    @Override
    public TransferStatus getTransferStatus(String transferId) {
        List<Transaction> transactionList = transactionOutputPort.findByTransferId(transferId);

        if (transactionList.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data found by transferId");

        String transferStatus = "TRANSACTION_PROCESSING";

        if (isTransferCompleted(transactionList))
            transferStatus = "TRANSACTION_COMPLETED";
        else if (isTransferFailed(transactionList))
            transferStatus = "TRANSACTION_FAILED";

        return TransferStatus.builder()
                .status(transferStatus)
                .transferId(transferId)
                .build();
    }
}