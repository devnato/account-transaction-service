package com.account.domain.model;

import lombok.*;

import java.util.Date;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {

    private Long id;
    private Long userId;
    private Double amount;
    private Double fee;
    private Double amountWithFeeDiscount;
    private Double refund;
    private String status;
    private String type;
    private Date date;
    private String paymentInfoId;
    private String transferId;

}
