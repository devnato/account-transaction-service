package com.account.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTransferStatus {

    private String paymentInfoId;
    private String status;

}
