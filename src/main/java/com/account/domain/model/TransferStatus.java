package com.account.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferStatus {

    private String transferId;
    private String status;

}
