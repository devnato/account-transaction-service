package com.account.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferData {

    private Long userId;
    private Double amount;

}
