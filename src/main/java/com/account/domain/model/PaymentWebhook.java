package com.account.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentWebhook {

    private String id;
    private String type;
    private PaymentWebhookData data;

}
