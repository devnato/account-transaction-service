package com.account.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private Long id;
    private String name;
    private String routingNumber;
    private String surname;
    private String nationalIdentificationNumber;
    private String accountNumber;
    private String bankName;
    private Long userId;
}
