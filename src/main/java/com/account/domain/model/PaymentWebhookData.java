package com.account.domain.model;

import lombok.*;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentWebhookData {

    private String paymentInfoId;
    private Double amountReceived;

}
