package com.account.infrastructure.adapters.input.rest.data.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferWebhookResponse {

    private String paymentInfoId;
    private String status;

}
