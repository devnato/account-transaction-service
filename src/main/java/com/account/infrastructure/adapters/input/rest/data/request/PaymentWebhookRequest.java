package com.account.infrastructure.adapters.input.rest.data.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentWebhookRequest {

    private String id;
    private String type;
    private PaymentWebhookData data;

}
