package com.account.infrastructure.adapters.input.rest.data.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferInfoResponse {

    private String transferId;
    private String status;

}
