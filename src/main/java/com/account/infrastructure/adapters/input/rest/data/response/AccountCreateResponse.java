package com.account.infrastructure.adapters.input.rest.data.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreateResponse {

    private Long id;
    private String name;
    private String surname;
    private String nationalIdentificationNumber;
    private String accountNumber;
    private Long userId;

}
