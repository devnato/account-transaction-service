package com.account.infrastructure.adapters.input.rest.controllers;

import com.account.application.ports.input.*;
import com.account.domain.model.*;
import com.account.infrastructure.adapters.input.rest.data.request.AccountCreateRequest;
import com.account.infrastructure.adapters.input.rest.data.request.TransferRequest;
import com.account.infrastructure.adapters.input.rest.data.request.PaymentWebhookRequest;
import com.account.infrastructure.adapters.input.rest.data.response.AccountCreateResponse;
import com.account.infrastructure.adapters.input.rest.data.response.TransferInfoResponse;
import com.account.infrastructure.adapters.input.rest.data.response.TransactionPageableResponse;
import com.account.infrastructure.adapters.input.rest.data.response.TransferWebhookResponse;
import com.account.infrastructure.adapters.input.rest.mapper.AccountRestMapper;
import com.account.infrastructure.adapters.input.rest.mapper.PaymentWebhookRestMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/accounts")
@RequiredArgsConstructor
public class AccountRestAdapter {

    private final CreateAccountUseCase createAccountUseCase;
    private final ListAccountsUseCase listAccountsUseCase;
    private final GetTransferStatusUseCase getTransferStatusUseCase;
    private final TransferAccountUseCase transferAccountUseCase;
    private final ListTransactionsUseCase listTransactionsUseCase;
    private final ProcessPaymentWebhookUseCase processPaymentWebhookUseCase;

    private final AccountRestMapper accountRestMapper;
    private final PaymentWebhookRestMapper paymentWebhookRestMapper;

    @PostMapping
    public ResponseEntity<AccountCreateResponse> createAccount(@RequestBody @Valid AccountCreateRequest accountCreateRequest){
        Account account = accountRestMapper.toAccount(accountCreateRequest);
        account = createAccountUseCase.createAccount(account);
        return new ResponseEntity<>(accountRestMapper.toAccountCreateResponse(account), HttpStatus.CREATED);
    }

    @PostMapping(value = "/transfer")
    public ResponseEntity<TransferInfoResponse> transfer(@RequestBody @Valid TransferRequest transferRequest){
        TransferData transferData = accountRestMapper.toTransferData(transferRequest);
        Transaction transaction = transferAccountUseCase.transferToBankAccount(transferData);
        TransferInfoResponse transferInfoResponse = accountRestMapper.toTransferInfoResponse(transaction);
        return new ResponseEntity<>(transferInfoResponse, HttpStatus.CREATED);
    }

    @PostMapping(value = "/update-transfer-transaction")
    public ResponseEntity<TransferWebhookResponse> updateTransactionTransaction(@RequestBody @Valid PaymentWebhookRequest paymentWebhookRequest){
        PaymentWebhook paymentWebhook = paymentWebhookRestMapper.toPaymentWebhook(paymentWebhookRequest);
        PaymentTransferStatus transferStatus = processPaymentWebhookUseCase.proccessPaymentWebhook(paymentWebhook);
        TransferWebhookResponse transactionInfoResponse = accountRestMapper.toTransferWebhookResponse(transferStatus);
        return new ResponseEntity<>(transactionInfoResponse, HttpStatus.CREATED);
    }

    @GetMapping ResponseEntity<List<AccountCreateResponse>> listAccounts() {
        List<Account> accounts = listAccountsUseCase.listAccounts();
        return ResponseEntity.ok(accounts.stream().map(accountRestMapper::toAccountCreateResponse).collect(Collectors.toList()));
    }

    @GetMapping(value = "/transactions")
    public TransactionPageableResponse getAllTransactions(
            @RequestParam(value = "amount", required = false) Double amount,
            @RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ){
        TransactionPageable transactionPageable = listTransactionsUseCase.listAccounts(amount, pageNo, pageSize, sortBy, sortDir);
         return accountRestMapper.toTransactionPageableResponse(transactionPageable);
    }

    @GetMapping(value = "/transfers/{transferId}/status")
    public ResponseEntity<TransferStatus> getTransactionBy(@PathVariable String transferId){
        TransferStatus transferStatus = getTransferStatusUseCase.getTransferStatus(transferId);
        TransferInfoResponse transferInfoResponse = accountRestMapper.toTransferInfoResponse(transferStatus);
        return ResponseEntity.ok(transferStatus);
    }

}
