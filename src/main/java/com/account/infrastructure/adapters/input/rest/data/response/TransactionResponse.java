package com.account.infrastructure.adapters.input.rest.data.response;

import lombok.*;

import java.util.Date;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponse {

    private Long id;
    private String type;
    private String status;
    private Double amount;
    private Double fee;
    private Double amountWithFeeDiscount;
    private Double refund;
    private Date date;
    private String paymentInfoId;
    private String transferId;

}
