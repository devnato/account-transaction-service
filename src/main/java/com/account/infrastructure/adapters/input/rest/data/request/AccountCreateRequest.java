package com.account.infrastructure.adapters.input.rest.data.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreateRequest {

    @NotEmpty(message = "Name may not be empty")
    private String name;

    @NotEmpty(message = "Surname may not be empty")
    private String surname;

    @NotEmpty(message = "Routing number may not be empty")
    private String routingNumber;

    @NotEmpty(message = "National identification number may not be empty")
    private String nationalIdentificationNumber;

    @NotEmpty(message = "Account number may not be empty")
    private String accountNumber;

    @NotEmpty(message = "Bank name may not be empty")
    private String bankName;

    private Long userId;
}
