package com.account.infrastructure.adapters.input.rest.mapper;

import com.account.domain.model.PaymentWebhook;
import com.account.infrastructure.adapters.input.rest.data.request.PaymentWebhookRequest;
import org.mapstruct.Mapper;

@Mapper
public interface PaymentWebhookRestMapper {

    PaymentWebhook toPaymentWebhook(PaymentWebhookRequest paymentWebhookRequest);

}
