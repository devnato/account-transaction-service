package com.account.infrastructure.adapters.input.rest.mapper;

import com.account.domain.model.*;
import com.account.infrastructure.adapters.input.rest.data.request.AccountCreateRequest;
import com.account.infrastructure.adapters.input.rest.data.request.TransferRequest;
import com.account.infrastructure.adapters.input.rest.data.response.*;
import org.mapstruct.Mapper;

@Mapper
public interface AccountRestMapper {

    Account toAccount(AccountCreateRequest accountCreateRequest);

    AccountCreateResponse toAccountCreateResponse(Account account);

    Transaction toTransaction(TransferRequest transferRequest);

    TransferData toTransferData(TransferRequest transferRequest);

    TransferInfoResponse toTransferInfoResponse(Transaction transaction);

    TransferInfoResponse toTransferInfoResponse(TransferStatus transferStatus);

    TransferWebhookResponse toTransferWebhookResponse(PaymentTransferStatus transferStatus);

    TransactionResponse toTransactionResponse(Transaction transaction);

    TransactionPageableResponse toTransactionPageableResponse(TransactionPageable transactionPageable);
}
