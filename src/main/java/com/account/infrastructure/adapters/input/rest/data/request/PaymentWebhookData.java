package com.account.infrastructure.adapters.input.rest.data.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentWebhookData {

    private String paymentInfoId;
    private Double amountReceived;

}
