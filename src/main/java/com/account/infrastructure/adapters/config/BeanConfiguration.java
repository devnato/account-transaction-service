package com.account.infrastructure.adapters.config;

import com.account.domain.service.AccountService;
import com.account.infrastructure.adapters.output.api.PaymentClientAdapter;
import com.account.infrastructure.adapters.output.api.WalletClientAdapter;
import com.account.infrastructure.adapters.output.mapper.AccountApiMapper;
import com.account.infrastructure.adapters.output.mapper.PaymentApiMapper;
import com.account.infrastructure.adapters.output.persistence.AccountPersistenceAdapter;
import com.account.infrastructure.adapters.output.persistence.TransactionPersistenceAdapter;
import com.account.infrastructure.adapters.output.persistence.mapper.AccountPersistenceMapper;
import com.account.infrastructure.adapters.output.persistence.mapper.TransactionPersistenceMapper;
import com.account.infrastructure.adapters.output.persistence.repository.AccountRepository;
import com.account.infrastructure.adapters.output.persistence.repository.TransactionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public AccountPersistenceAdapter accountPersistenceAdapter(AccountRepository accountRepository,
                                                               AccountPersistenceMapper accountPersistenceMapper) {
        return new AccountPersistenceAdapter(accountRepository, accountPersistenceMapper);
    }

    @Bean
    public TransactionPersistenceAdapter transactionPersistenceAdapter(TransactionRepository transactionRepository,
                                                                   TransactionPersistenceMapper transactionPersistenceMapper) {
        return new TransactionPersistenceAdapter(transactionRepository, transactionPersistenceMapper);
    }

    @Bean
    public AccountService accountService(AccountPersistenceAdapter accountPersistenceAdapter,
                                         WalletClientAdapter walletClientAdapter,
                                         AccountApiMapper accountApiMapper,
                                         PaymentApiMapper paymentApiMapper,
                                         PaymentClientAdapter paymentClientAdapter,
                                         TransactionPersistenceAdapter transactionPersistenceAdapter) {
        return new AccountService(accountPersistenceAdapter,
                                    transactionPersistenceAdapter,
                                    walletClientAdapter,
                                    paymentClientAdapter,
                                    accountApiMapper,
                                    paymentApiMapper);
    }

}
