package com.account.infrastructure.adapters.output.api.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentResponse {

    private RequestInfoResponse requestInfo;
    private PaymentInfoResponse paymentInfo;

}
