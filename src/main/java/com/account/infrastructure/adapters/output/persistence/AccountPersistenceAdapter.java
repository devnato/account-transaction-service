package com.account.infrastructure.adapters.output.persistence;

import com.account.application.ports.output.AccountOutputPort;
import com.account.domain.model.Account;
import com.account.infrastructure.adapters.output.persistence.entity.AccountEntity;
import com.account.infrastructure.adapters.output.persistence.mapper.AccountPersistenceMapper;
import com.account.infrastructure.adapters.output.persistence.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class AccountPersistenceAdapter implements AccountOutputPort {

    private final AccountRepository accountRepository;

    private final AccountPersistenceMapper accountPersistenceMapper;

    @Override
    public Account saveAccount(Account account) {
        AccountEntity accountEntity = accountPersistenceMapper.toAccountEntity(account);
        accountEntity = accountRepository.save(accountEntity);
        return accountPersistenceMapper.toAccount(accountEntity);
    }

    @Override
    public Account findByUserId(Long userId) {
        AccountEntity accountEntity = accountRepository.findByUserId(userId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User account not found"));
        return accountPersistenceMapper.toAccount(accountEntity);
    }

    @Override
    public List<Account> listAccounts() {
        return accountRepository.findAll()
                .stream()
                .map(accountPersistenceMapper::toAccount)
                .collect(Collectors.toList());
    }
}
