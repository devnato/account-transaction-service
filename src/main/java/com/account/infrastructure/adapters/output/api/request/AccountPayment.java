package com.account.infrastructure.adapters.output.api.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountPayment {

    private String accountNumber;
    private String currency;
    private String routingNumber;

}
