package com.account.infrastructure.adapters.output.persistence.mapper;

import com.account.domain.model.Transaction;
import com.account.infrastructure.adapters.output.persistence.entity.TransactionEntity;
import org.mapstruct.Mapper;

@Mapper
public interface TransactionPersistenceMapper {

    TransactionEntity toTransactionEntity(Transaction transaction);

    Transaction toTransaction(TransactionEntity transaction);
}
