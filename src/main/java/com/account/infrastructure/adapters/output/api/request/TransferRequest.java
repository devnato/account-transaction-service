package com.account.infrastructure.adapters.output.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferRequest {

    @JsonProperty("user_id")
    private Long userId;
    private Double amount;

}
