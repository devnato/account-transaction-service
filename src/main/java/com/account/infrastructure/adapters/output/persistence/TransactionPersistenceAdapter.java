package com.account.infrastructure.adapters.output.persistence;

import com.account.application.ports.output.TransactionOutputPort;
import com.account.domain.model.Transaction;
import com.account.domain.model.TransactionPageable;
import com.account.infrastructure.adapters.output.persistence.entity.TransactionEntity;
import com.account.infrastructure.adapters.output.persistence.mapper.TransactionPersistenceMapper;
import com.account.infrastructure.adapters.output.persistence.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TransactionPersistenceAdapter implements TransactionOutputPort {

    private final TransactionRepository transactionRepository;

    private final TransactionPersistenceMapper transactionPersistenceMapper;

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        TransactionEntity transactionEntity = transactionPersistenceMapper.toTransactionEntity(transaction);
        transactionEntity = transactionRepository.save(transactionEntity);
        transaction.setId(transactionEntity.getId());
        return transaction;
    }

    @Override
    public TransactionPageable listTransactions(Double amount, int pageNo, int pageSize, String sortBy, String sortDir) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
                : Sort.by(sortBy).descending();

        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<TransactionEntity> transactions;

        if (amount == null)
            transactions = transactionRepository.findAll(pageable);
        else
            transactions = transactionRepository.findByAmount(amount, pageable);

        List<TransactionEntity> listOfTransactions = transactions.getContent();
        List<Transaction> content= listOfTransactions
                .stream().map(transactionPersistenceMapper::toTransaction).collect(Collectors.toList());

        TransactionPageable transactionPageable = new TransactionPageable();
        transactionPageable.setContent(content);
        transactionPageable.setPageNo(transactions.getNumber());
        transactionPageable.setPageSize(transactions.getSize());
        transactionPageable.setTotalElements(transactions.getTotalElements());
        transactionPageable.setTotalPages(transactions.getTotalPages());
        transactionPageable.setLast(transactions.isLast());

        return transactionPageable;
    }

    @Override
    public List<Transaction> findByPaymentInfoId(String paymentInfoId) {
        List<TransactionEntity> transactionEntityList = transactionRepository.findByPaymentInfoId(paymentInfoId);
        List<Transaction> transactionList = transactionEntityList
                .stream()
                .map(transactionPersistenceMapper::toTransaction)
                .collect(Collectors.toList());
        return transactionList;
    }

    @Override
    public List<Transaction> findByTransferId(String transferId) {
        List<TransactionEntity> transactionEntityList = transactionRepository.findByTransferIdOrderByIdDesc(transferId);
        List<Transaction> transactionList = transactionEntityList
                .stream()
                .map(transactionPersistenceMapper::toTransaction)
                .collect(Collectors.toList());
        return transactionList;
    }
}
