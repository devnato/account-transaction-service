package com.account.infrastructure.adapters.output.api.config;

import com.account.infrastructure.adapters.output.api.PaymentApiRetry;
import com.account.infrastructure.adapters.output.api.PaymentErrorDecoder;
import feign.Retryer;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class FeignPaymentConfiguration extends FeignDefaultConfiguration{

    @Bean
    public ErrorDecoder errorDecoder() {
        return new PaymentErrorDecoder();
    }

    @Bean
    public Retryer retryer() {
        return new PaymentApiRetry();
    }

}
