package com.account.infrastructure.adapters.output.persistence.repository;

import com.account.infrastructure.adapters.output.persistence.entity.TransactionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {

    Page<TransactionEntity> findByAmount(Double amount, Pageable pageable);

    List<TransactionEntity> findByPaymentInfoId(String paymentInfoId);

    List<TransactionEntity> findByTransferIdOrderByIdDesc(String transferId);

}
