package com.account.infrastructure.adapters.output.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferResponse {

    @JsonProperty("wallet_transaction_id")
    private Long walletTransactionId;

    @JsonProperty("user_id")
    private Long userId;

    private Double amount;

}
