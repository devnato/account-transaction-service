package com.account.infrastructure.adapters.output.api;

import com.account.application.ports.output.PaymentServiceClient;
import com.account.infrastructure.adapters.output.api.config.FeignPaymentConfiguration;
import com.account.infrastructure.adapters.output.api.request.PaymentRequest;
import com.account.infrastructure.adapters.output.api.response.PaymentResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Service
@FeignClient(name = "payment-service", url = "${payment.api.url}", configuration = FeignPaymentConfiguration.class)
public interface PaymentClientAdapter extends PaymentServiceClient {

    @Override
    @RequestMapping(method = POST, consumes = "application/json")
    PaymentResponse payment(@RequestBody PaymentRequest paymentRequest);


}