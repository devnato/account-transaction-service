package com.account.infrastructure.adapters.output.api.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest {

    private SourcePayment source;
    private DestinationPayment destination;
    private Double amount;

}
