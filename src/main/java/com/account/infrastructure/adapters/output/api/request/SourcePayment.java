package com.account.infrastructure.adapters.output.api.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SourcePayment {

    private String type;
    private SourceInformationPayment sourceInformation;
    private AccountPayment account;

}
