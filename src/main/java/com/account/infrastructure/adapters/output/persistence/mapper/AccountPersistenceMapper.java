package com.account.infrastructure.adapters.output.persistence.mapper;

import com.account.domain.model.Account;
import com.account.infrastructure.adapters.output.persistence.entity.AccountEntity;
import org.mapstruct.Mapper;

@Mapper
public interface AccountPersistenceMapper {

    AccountEntity toAccountEntity(Account account);

    Account toAccount(AccountEntity accountEntity);
}
