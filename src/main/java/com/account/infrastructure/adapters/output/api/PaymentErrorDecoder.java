package com.account.infrastructure.adapters.output.api;

import com.account.infrastructure.adapters.output.api.exception.PaymentDeclinedException;
import com.account.infrastructure.adapters.output.api.exception.PaymentRequestException;
import com.account.infrastructure.adapters.output.api.response.ExceptionMessage;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.RetryableException;
import feign.codec.ErrorDecoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class PaymentErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        String body;
        ExceptionMessage exceptionMessage = null;
        try {
            body = new BufferedReader(response.body().asReader(StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            exceptionMessage = mapper.readValue(body, ExceptionMessage.class);
        } catch (IOException ignore) {}
        if (exceptionMessage.getRequestInfo().getError().equals("bank rejected payment")) {
            throw new PaymentDeclinedException(exceptionMessage.getRequestInfo().getError(), exceptionMessage.getPaymentInfo().getId());
        }

        if (exceptionMessage.getRequestInfo().getError().equals("timeout")) {
            return new RetryableException(500, "Server error", response.request().httpMethod(), null, response.request());
        }

        return errorDecoder.decode(s, response);
    }

}