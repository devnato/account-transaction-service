package com.account.infrastructure.adapters.output.api.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentInfoResponse {

    private String amount;
    private String id;

}
