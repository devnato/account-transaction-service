package com.account.infrastructure.adapters.output.persistence.entity;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "transaction")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    private String status;
    private Double amount;
    private Double fee;
    private Double amountWithFeeDiscount;
    private Double refund;
    private Date date;
    private String paymentInfoId;
    private String transferId;
    private Long userId;

}
