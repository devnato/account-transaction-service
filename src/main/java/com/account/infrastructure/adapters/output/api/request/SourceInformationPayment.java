package com.account.infrastructure.adapters.output.api.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SourceInformationPayment {

    private String name;

}
