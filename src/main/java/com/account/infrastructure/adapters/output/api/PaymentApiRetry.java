package com.account.infrastructure.adapters.output.api;

import com.account.infrastructure.adapters.output.api.exception.PaymentRequestException;
import feign.RetryableException;
import feign.Retryer;

public class PaymentApiRetry implements Retryer {

    private final int maxAttempts;
    private final long backoff;
    int attempt;

    public PaymentApiRetry() {
        this(2000, 3);
    }

    public PaymentApiRetry(long backoff, int maxAttempts) {
        this.backoff = backoff;
        this.maxAttempts = maxAttempts;
        this.attempt = 1;
    }

    public void continueOrPropagate(RetryableException e) {
        if (attempt++ >= maxAttempts) {
            throw new PaymentRequestException("Number of attempts exceeded");
        }

        try {
            System.out.println("Payment request try");
            Thread.sleep(backoff);
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Retryer clone() {
        return new PaymentApiRetry(backoff, maxAttempts);
    }
}