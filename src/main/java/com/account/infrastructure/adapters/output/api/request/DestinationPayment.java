package com.account.infrastructure.adapters.output.api.request;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DestinationPayment {

    private String name;
    private AccountPayment account;

}
