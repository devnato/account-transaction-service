package com.account.infrastructure.adapters.output.api.exception;

public class PaymentDeclinedException extends RuntimeException implements PaymentException{
    String paymentId;
    public PaymentDeclinedException(String errorMessage, String paymentId) {
        super(errorMessage);
        this.paymentId = paymentId;
    }

    public PaymentDeclinedException(String errorMessage) {
        super(errorMessage);
    }

    public String getPaymentId() {
        return this.paymentId;
    }
}
