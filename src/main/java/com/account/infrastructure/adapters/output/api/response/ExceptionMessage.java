package com.account.infrastructure.adapters.output.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExceptionMessage {

    private RequestInfoResponse requestInfo;
    private PaymentInfoResponse paymentInfo;

}
