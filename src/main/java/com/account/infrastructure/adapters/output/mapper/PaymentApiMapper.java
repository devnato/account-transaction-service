package com.account.infrastructure.adapters.output.mapper;

import com.account.domain.model.Account;
import com.account.domain.model.TransferData;
import com.account.infrastructure.adapters.output.api.request.*;
import org.mapstruct.Mapper;

@Mapper
public class PaymentApiMapper {

    public PaymentRequest toPaymentRequest(Account account, TransferData transferData) {
        PaymentRequest paymentRequest = new PaymentRequest();

        //SOURCE PAYMENT DATA
        SourcePayment sourcePayment = new SourcePayment();

        AccountPayment sourceAccountPayment = new AccountPayment();
        sourceAccountPayment.setAccountNumber("0245253419");
        sourceAccountPayment.setCurrency("USD");
        sourceAccountPayment.setRoutingNumber("028444018");

        SourceInformationPayment sourceInformation = new SourceInformationPayment();
        sourceInformation.setName("ONTOP INC");

        sourcePayment.setType("COMPANY");
        sourcePayment.setAccount(sourceAccountPayment);
        sourcePayment.setSourceInformation(sourceInformation);

        //DESTINATION PAYMENT DATA
        AccountPayment destinationAccountPayment = new AccountPayment();
        destinationAccountPayment.setAccountNumber(account.getAccountNumber());
        destinationAccountPayment.setCurrency("USD");
        destinationAccountPayment.setRoutingNumber(account.getRoutingNumber());

        DestinationPayment destinationPayment = new DestinationPayment();
        destinationPayment.setAccount(destinationAccountPayment);
        destinationPayment.setName(account.getName() + " " + account.getSurname());

        paymentRequest.setSource(sourcePayment);
        paymentRequest.setDestination(destinationPayment);
        paymentRequest.setAmount(transferData.getAmount());

        return paymentRequest;

    }
}
