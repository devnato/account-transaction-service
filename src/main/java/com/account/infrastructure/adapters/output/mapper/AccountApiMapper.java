package com.account.infrastructure.adapters.output.mapper;

import com.account.domain.model.Transaction;
import com.account.domain.model.TransferData;
import com.account.domain.model.WithdrawData;
import com.account.infrastructure.adapters.output.api.request.TransferRequest;
import org.mapstruct.Mapper;

@Mapper
public interface AccountApiMapper {

    TransferRequest toTransferRequest(Transaction transaction);
    TransferRequest toTransferRequest(TransferData transferData);
    TransferRequest toTransferRequest(WithdrawData withdrawData);
    WithdrawData toWithdrawData(TransferData transferData);

}
