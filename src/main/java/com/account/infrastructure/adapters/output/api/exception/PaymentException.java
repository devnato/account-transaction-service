package com.account.infrastructure.adapters.output.api.exception;

public interface PaymentException {

    String getPaymentId();
}
