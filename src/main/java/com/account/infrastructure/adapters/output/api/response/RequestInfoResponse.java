package com.account.infrastructure.adapters.output.api.response;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestInfoResponse {

    private String status;
    private String error;

}
