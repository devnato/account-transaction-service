package com.account.infrastructure.adapters.output.api;

import com.account.application.ports.output.WalletServiceClient;
import com.account.infrastructure.adapters.output.api.config.FeignDefaultConfiguration;
import com.account.infrastructure.adapters.output.api.request.TransferRequest;
import com.account.infrastructure.adapters.output.api.response.TransferResponse;
import com.account.infrastructure.adapters.output.api.response.WalletBalanceResponse;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Service
@FeignClient(name = "wallet-service", url = "${wallet.api.url}", configuration = FeignDefaultConfiguration.class)
public interface WalletClientAdapter extends WalletServiceClient {

    @Override
    @RequestMapping(method = GET, value = "/balance?user_id={user_id}", consumes = "application/json")
    WalletBalanceResponse balance(@PathVariable("user_id") Long user_id);

    @Override
    @RequestMapping(method = POST, value = "/transactions", consumes = "application/json")
    @CircuitBreaker(name = "transactions-cb", fallbackMethod = "transactionsFallback")
    TransferResponse transactions(@RequestBody TransferRequest transferRequest);

    default TransferResponse transactionsFallback(TransferRequest transferRequest, Throwable t) {
        //TODO put in a log factory
        System.out.println("transactionsFallback called for user "+ transferRequest.getUserId() + ", " + t.getMessage());
        return null;
    }

}
