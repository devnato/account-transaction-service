package com.account.enums;

public enum Status {

    CREATED,
    ACTIVATED,
    HOLD,
    DEBIT_DECLINED
}
