package com.account.enums;

public enum TransactionType {

    CREDIT,
    DEBIT
}
